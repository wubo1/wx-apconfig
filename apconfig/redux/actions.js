const store = require('./index');
const actionTypes = require('./actionTypes');

// wifiList
module.exports.updateWifiList = (wifiList) => {
  store.dispatch({
    type: actionTypes.UPDATE_WIFI_LIST,
    payload: {
      wifiList,
    },
  });
};

module.exports.resetWifiList = () => {
  store.dispatch({
    type: actionTypes.RESET_WIFI_LIST,
    payload: {},
  });
};