Component({
  methods: {
    onBottomButtonClick(e) {
      switch (e.detail.btn.id) {
        case 'complete':
          this.triggerEvent('complete', {}, {});
          break;
      }
    },
  },
});
