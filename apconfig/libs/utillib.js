module.exports.delay = (timeout) => new Promise((resolve) => setTimeout(() => resolve(), timeout));

module.exports.getErrorMsg = (err) => {
  if (!err) return;

  if (typeof err === 'string') return err;

  let message = err.msg || err.message || err.errMsg || err.Message || '';

  if (message && err.reqId) {
    message += `(${err.reqId})`;
  }

  return message;
};
