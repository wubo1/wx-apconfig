//app.js
App({
  onLaunch: function () {
    const systemInfo = wx.getSystemInfoSync();
    this.globalData = {
      isIpx: (systemInfo.screenHeight / systemInfo.screenWidth) > 1.86,
      isAndroid: systemInfo.platform.toLowerCase().indexOf('android') > -1,
      isIOS: systemInfo.platform.toLowerCase().indexOf('ios') > -1,
    };
  },
  globalData: {
  }
});